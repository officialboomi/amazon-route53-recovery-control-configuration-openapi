# Amazon Route53 Recovery Control Configuration Connector
Recovery Control Configuration API Reference for Amazon Route 53 Application Recovery Controller

Documentation: https://docs.aws.amazon.com/route53-recovery-control-config

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/route53-recovery-control-config/2020-11-02/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations

All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

